package me.sargunvohra.mcmods.composableautomation.content

import net.minecraft.state.property.EnumProperty
import net.minecraft.util.StringIdentifiable

enum class RotatorMode : StringIdentifiable {
    CLOCKWISE,
    COUNTER_CLOCKWISE;

    override fun asString() = toString().toLowerCase()

    companion object {
        val prop = EnumProperty.create("mode", RotatorMode::class.java)!!
    }
}
