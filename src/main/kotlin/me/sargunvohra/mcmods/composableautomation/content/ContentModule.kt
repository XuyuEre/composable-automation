package me.sargunvohra.mcmods.composableautomation.content

import me.sargunvohra.mcmods.composableautomation.InitModule
import me.sargunvohra.mcmods.composableautomation.common.id
import net.minecraft.item.Item
import net.minecraft.item.ItemGroup
import net.minecraft.util.registry.Registry

@Suppress("MemberVisibilityCanBePrivate")
object ContentModule : InitModule {

    val chute = id("chute")
    val rotator = id("rotator")
    val dynamicLamp = id("dynamic_lamp")
    val netherDust = id("nether_dust")

    override fun initCommon() {
        Registry.register(Registry.BLOCK, chute, ChuteBlock)
        Registry.register(Registry.ITEM, chute, ChuteBlock.ChuteItem)
        Registry.register(Registry.BLOCK_ENTITY, chute, ChuteBlockEntity.type)

        Registry.register(Registry.BLOCK, rotator, RotatorBlock)
        Registry.register(Registry.ITEM, rotator, RotatorBlock.RotatorItem)

        Registry.register(Registry.BLOCK, dynamicLamp, DynamicLampBlock)
        Registry.register(Registry.ITEM, dynamicLamp, DynamicLampBlock.DynamicLampItem)

        Registry.register(Registry.ITEM, netherDust, Item(Item.Settings().itemGroup(ItemGroup.MATERIALS)))
    }
}
