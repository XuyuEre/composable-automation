package me.sargunvohra.mcmods.composableautomation.common

import net.minecraft.entity.ItemEntity
import net.minecraft.inventory.Inventories
import net.minecraft.inventory.Inventory
import net.minecraft.item.ItemStack
import net.minecraft.nbt.CompoundTag
import net.minecraft.util.DefaultedList
import net.minecraft.util.Identifier
import net.minecraft.util.math.Vec3d
import net.minecraft.world.World

fun id(name: String) = Identifier("composableautomation", name)

fun Inventory.invToTag(tag: CompoundTag): CompoundTag {
    val items = (0 until invSize).map { i -> getInvStack(i) }.toTypedArray()
    return Inventories.toTag(tag, DefaultedList.create(ItemStack.EMPTY, *items))
}

fun Inventory.invFromTag(tag: CompoundTag) {
    val savedContent = DefaultedList.create(invSize, ItemStack.EMPTY)
    Inventories.fromTag(tag, savedContent)
    savedContent.forEachIndexed(this::setInvStack)
}

fun ItemStack.spawn(world: World, pos: Vec3d): ItemEntity? {
    val newEntity = ItemEntity(world, pos.x, pos.y, pos.z, this)
    return if (world.spawnEntity(newEntity))
        newEntity
    else null
}
