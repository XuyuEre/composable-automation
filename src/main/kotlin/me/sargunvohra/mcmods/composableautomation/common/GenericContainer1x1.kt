package me.sargunvohra.mcmods.composableautomation.common

import net.minecraft.container.Container
import net.minecraft.container.Slot
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.entity.player.PlayerInventory
import net.minecraft.inventory.BasicInventory
import net.minecraft.inventory.Inventory
import net.minecraft.item.ItemStack

class GenericContainer1x1(
    syncId: Int,
    playerInv: PlayerInventory,
    private val inventory: Inventory = BasicInventory(1)
) : Container(null, syncId) {

    init {
        Container.checkContainerSize(inventory, 1)
        inventory.onInvOpen(playerInv.player)

        this.addSlot(Slot(inventory, 0, 80, 35))

        for (row in 0 until 3) {
            for (col in 0 until 9) {
                this.addSlot(Slot(playerInv, col + row * 9 + 9, 8 + col * 18, 84 + row * 18))
            }
        }

        for (col in 0 until 9) {
            this.addSlot(Slot(playerInv, col, 8 + col * 18, 142))
        }
    }

    override fun canUse(player: PlayerEntity): Boolean {
        return this.inventory.canPlayerUseInv(player)
    }

    override fun transferSlot(player: PlayerEntity, slotIndex: Int): ItemStack {
        var ret = ItemStack.EMPTY
        val slot = this.slotList[slotIndex] ?: null
        if (slot?.hasStack() == true) {
            val slotStack = slot.stack
            ret = slotStack.copy()
            when (slotIndex) {
                0 -> if (!this.insertItem(slotStack, 1, 37, true)) {
                    return ItemStack.EMPTY
                }
                else -> if (!this.insertItem(slotStack, 0, 1, false)) {
                    return ItemStack.EMPTY
                }
            }

            if (slotStack.isEmpty) {
                slot.stack = ItemStack.EMPTY
            } else {
                slot.markDirty()
            }

            if (slotStack.amount == ret.amount) {
                return ItemStack.EMPTY
            }

            slot.onTakeItem(player, slotStack)
        }

        return ret
    }

    override fun close(playerEntity_1: PlayerEntity) {
        super.close(playerEntity_1)
        this.inventory.onInvClose(playerEntity_1)
    }
}
