package me.sargunvohra.mcmods.composableautomation.common

import me.sargunvohra.mcmods.composableautomation.InitModule
import net.fabricmc.fabric.api.client.screen.ScreenProviderRegistry
import net.fabricmc.fabric.api.container.ContainerProviderRegistry
import net.minecraft.inventory.Inventory

object CommonModule : InitModule {

    val container1x1Id = id("generic_1x1_container")

    override fun initCommon() {
        ContainerProviderRegistry.INSTANCE.registerFactory(container1x1Id) { syncId, _, player, buf ->
            val pos = buf.readBlockPos()
            val entity = player.world.getBlockEntity(pos) as Inventory
            GenericContainer1x1(syncId, player.inventory, entity)
        }
    }

    override fun initClient() {
        ScreenProviderRegistry.INSTANCE.registerFactory(container1x1Id) { syncId, _, player, buf ->
            val pos = buf.readBlockPos()
            val entity = player.world.getBlockEntity(pos) as Inventory
            val container =
                GenericContainer1x1(syncId, player.inventory, entity)
            ContainerScreen1x1(
                container,
                player.inventory,
                buf.readTextComponent()
            )
        }
    }
}
