package me.sargunvohra.mcmods.composableautomation.tweak

import me.sargunvohra.mcmods.composableautomation.common.FakePlayer
import net.minecraft.block.DispenserBlock
import net.minecraft.block.dispenser.ItemDispenserBehavior
import net.minecraft.entity.passive.AnimalEntity
import net.minecraft.item.ItemStack
import net.minecraft.util.Hand
import net.minecraft.util.math.BlockPointer
import net.minecraft.util.math.BoundingBox

object DispenserFeeding : ItemDispenserBehavior() {

    private val fallback = ItemDispenserBehavior()

    override fun dispenseStack(blockPointer: BlockPointer, stack: ItemStack): ItemStack {
        val targetPos = blockPointer.blockPos.offset(blockPointer.blockState.get(DispenserBlock.FACING))
        val success = blockPointer.world.getEntities(AnimalEntity::class.java, BoundingBox(targetPos))
            .firstOrNull { it.isBreedingItem(stack) }
            ?.let {
                val player = FakePlayer(blockPointer.world, "dispenser")
                player.setStackInHand(Hand.MAIN_HAND, stack)
                it.interact(player, Hand.MAIN_HAND)
            } ?: false
        return if (success) stack else fallback.dispense(blockPointer, stack)
    }
}
