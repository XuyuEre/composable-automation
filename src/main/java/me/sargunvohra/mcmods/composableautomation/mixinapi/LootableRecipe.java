package me.sargunvohra.mcmods.composableautomation.mixinapi;

import net.minecraft.item.ItemStack;
import net.minecraft.world.loot.LootManager;
import net.minecraft.world.loot.context.LootContext;

import java.util.List;

public interface LootableRecipe {
    List<ItemStack> craftBonus(LootManager lootManager, LootContext lootContext);
}
