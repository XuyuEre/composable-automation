package me.sargunvohra.mcmods.composableautomation.mixin;

import net.minecraft.fluid.Fluid;
import net.minecraft.tag.FluidTags;
import net.minecraft.tag.TagContainer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

@Mixin(FluidTags.class)
public interface AccessFluidTags {
    @Accessor
    static TagContainer<Fluid> getContainer() {
        throw new IllegalStateException();
    }
}
